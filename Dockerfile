FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# the ffmpeg imagemagick ghostscript are installed for previews (see #74)
RUN apt-get update && apt-get install -y smbclient p7zip-full libreoffice libmagickcore-6.q16-6-extra rar unrar && rm -r /var/cache/apt /var/lib/apt/lists

# allow imagemagik to generate PDF thumbnails (https://forum.cloudron.io/topic/6736/imagemagick-memory-allocation-failed)
COPY policy.xml /etc/ImageMagick-6/policy.xml

# renovate: datasource=github-tags depName=nextcloud/server versioning=semver extractVersion=^v(?<version>.+)$
ARG NEXTCLOUD_VERSION=31.0.1

RUN curl -L https://download.nextcloud.com/server/releases/nextcloud-${NEXTCLOUD_VERSION}.tar.bz2 | tar -xj --strip-components 1 -f - -C . && \
    chown -R www-data:www-data /app/code

# create config folder link to make the config survive updates
RUN rm -rf /app/code/config && ln -s /app/data/config /app/code/config && \
    mv /app/code/apps /app/pkg/apps_template && ln -s /app/data/apps /app/code/apps

# configure apache
# keep the prefork linking below a2enmod since it removes dangling mods-enabled (!)
# a2dismod perl is required for php mod to be able to use locales
RUN a2disconf other-vhosts-access-log && \
    echo "Listen 80" > /etc/apache2/ports.conf && \
    a2enmod rewrite env && \
    a2dismod perl && \
    rm /etc/apache2/sites-enabled/* && \
    sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf && \
    ln -sf /app/data/apache/mpm_prefork.conf /etc/apache2/mods-enabled/mpm_prefork.conf

COPY apache/nextcloud.conf /etc/apache2/sites-enabled/nextcloud.conf

RUN crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_filesize 5G && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP post_max_size 5G && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP memory_limit 512M && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.enable 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.enable_cli 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.interned_strings_buffer 32 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.max_accelerated_files 10000 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.memory_consumption 128 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.save_comments 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.revalidate_freq 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.save_path /run/nextcloud/sessions && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_divisor 100

# https://github.com/nextcloud/server/issues/32003 . Legacy RC4 must be activated for encryption to work on Ubuntu 22.04
RUN crudini --set /etc/ssl/openssl.cnf provider_sect legacy "legacy_sect" && \
    crudini --set /etc/ssl/openssl.cnf default_sect activate 1 && \
    crudini --set /etc/ssl/openssl.cnf legacy_sect activate 1

RUN sed -e 's/\(.*\)php_value upload_max_filesize.*/\1php_value upload_max_filesize 5G/' \
        -e 's/\(.*\)php_value post_max_size.*/\1php_value post_max_size 5G/' \
        /app/code/.htaccess > /app/pkg/htaccess.template

RUN ln -s /app/data/php.ini /etc/php/8.3/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.3/cli/conf.d/99-cloudron.ini

# note: there is also an auto-generated /app/data/.htaccess created by the install script for the data directory
# this one is for the code directory
RUN ln -sf /app/data/htaccess /app/code/.htaccess

# create occ alias
RUN echo "alias occ='sudo -u www-data -- php -f /app/code/occ'" >> /root/.bashrc

COPY apache/mpm_prefork.conf start.sh cron.sh preview-cleanup.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
