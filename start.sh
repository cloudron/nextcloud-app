#!/bin/bash

set -eu

readonly occ="sudo -u www-data php /app/code/occ"

export mail_from_sub=$(echo $CLOUDRON_MAIL_FROM | cut -d \@ -f 1)
export mail_domain_sub=$(echo $CLOUDRON_MAIL_FROM | cut -d \@ -f 2)

mkdir -p /run/nextcloud/sessions

if [[ -z "$(ls -A /app/data)" ]]; then
    echo "==> Detected first run"
    mkdir -p /app/data/config
    cp -rf /app/pkg/apps_template /app/data/apps

    # note: the install command patches this htaccess (this is for the code). it also generates /app/data/.htaccess for data-dir
    cp /app/pkg/htaccess.template /app/data/htaccess

    echo "==> Install nextcloud"
    # https://docs.nextcloud.com/server/14/admin_manual/configuration_server/occ_command.html#command-line-installation-label
    php /app/code/occ maintenance:install --database "pgsql" --database-name "${CLOUDRON_POSTGRESQL_DATABASE}"  --database-user "${CLOUDRON_POSTGRESQL_USERNAME}" --database-pass "${CLOUDRON_POSTGRESQL_PASSWORD}" --database-host "${CLOUDRON_POSTGRESQL_HOST}" --database-port ${CLOUDRON_POSTGRESQL_PORT} --admin-user "admin" --admin-pass "changeme" --data-dir "/app/data/data" -n
else
    NEW_APPS="/app/pkg/apps_template"
    OLD_APPS="/app/data/apps"

    echo "==> Updating apps"

    echo "==> Old apps:"
    ls "${NEW_APPS}/"
    ls "${OLD_APPS}/"

    for app in `find "${NEW_APPS}"/* -maxdepth 0 -type d -printf "%f\n"`; do
        echo "==> Update app: ${app}"
        rm -rf "${OLD_APPS}/${app}"
        cp -rf "${NEW_APPS}/${app}" "${OLD_APPS}"
    done

    echo "==> New apps:"
    ls "${NEW_APPS}/"
    ls "${OLD_APPS}/"

    # note: there is also an auto-generated /app/data/.htaccess created by the install script for the data directory
    # this one is for the code directory
    echo "==> Copying htaccess"
    cp /app/pkg/htaccess.template /app/data/htaccess
fi

# ensure symlink for scss files
rm -f /app/data/core && ln -s /app/code/core /app/data/core

# maybe we can use '/app/code/occ config:system:set htaccess.RewriteBase --value="/"'
# htaccess.RewriteBase has to be a path and non-empty string for nextcloud to generate redirect rules in htaccess
chown -R www-data:www-data /app/data/config /app/data/apps /app/data/htaccess
chown www-data:www-data /app/data
[[ -d /app/data/data ]] && chown www-data:www-data /app/data/data
echo "==> update config"
sudo -u www-data --preserve-env php <<'EOF'
<?php
    require_once "/app/data/config/config.php";
    $db = parse_url(getenv('CLOUDRON_POSTGRESQL_URL'));
    $runtime_config = array (
        'trusted_domains' => array ( 0 => getenv('CLOUDRON_APP_DOMAIN') ),
        'trusted_proxies' => array ( 0 => '172.18.0.1' ),
        'forcessl' => getenv('CLOUDRON'), // if unset/false, nextcloud sends a HSTS=0 header
        'mail_smtpmode' => 'smtp',
        'mail_smtpauth' => 1,
        'mail_sendmailmode' => 'smtp',
        'mail_smtpauthtype' => 'LOGIN',
        'mail_smtphost' => getenv('CLOUDRON_MAIL_SMTP_SERVER'),
        'mail_smtpport' => getenv('CLOUDRON_MAIL_SMTP_PORT'),
        'mail_smtpname' => getenv('CLOUDRON_MAIL_SMTP_USERNAME'),
        'mail_smtppassword' => getenv('CLOUDRON_MAIL_SMTP_PASSWORD'),
        'mail_from_address' => getenv('mail_from_sub'),
        'mail_smtpsecure' => '',
        'mail_domain' => getenv('mail_domain_sub'),
        'maintenance_window_start' => 1,
        'overwrite.cli.url' => getenv('CLOUDRON_APP_ORIGIN').'/',
        'overwritehost' => getenv('CLOUDRON_APP_DOMAIN'),
        'overwriteprotocol' => 'https',
        'log_type' => 'file',
        'logfile' => '/run/nextcloud/nextcloud.log',
        'loglevel' => 3,
        'dbtype' => 'pgsql',
        'dbname' => substr($db['path'], 1),
        'dbuser' => $db['user'],
        'dbpassword' => $db['pass'],
        'dbhost' => $db['host'],
        'dbtableprefix' => 'oc_',
        'updatechecker' => false,
        'updater.release.channel' => 'cloudron',
        'redis' => array(
            'host' => getenv('CLOUDRON_REDIS_HOST'),
            'port' => getenv('CLOUDRON_REDIS_PORT'),
            'password' => getenv('CLOUDRON_REDIS_PASSWORD')
        ),
        'memcache.local' => '\OC\Memcache\Redis',
        'memcache.locking' => '\OC\Memcache\Redis',
        'integrity.check.disabled' => true,
        'localstorage.allowsymlinks' => true, // https://forum.cloudron.io/topic/11096/since-last-update-error-in-nextcloud
        'htaccess.RewriteBase' => '/',
        'simpleSignUpLink.shown' => false,
        'dns_pinning' => false // https://forum.cloudron.io/topic/13188/nextcloud-oidc-integration/62
    );

    if (!$CONFIG['default_phone_region']) $CONFIG['default_phone_region'] = 'US';

    // default trash retention. min 7 days and up to 30 days. nc only cleans up when space is required
    if (!$CONFIG['trashbin_retention_obligation']) $CONFIG['trashbin_retention_obligation'] = '7,30';

    $CONFIG = array_replace($CONFIG, $runtime_config);
    file_put_contents("/app/data/config/config.php", "<?php\n\$CONFIG = " . var_export($CONFIG, true) . ";\n");
EOF

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

mkdir -p /app/data/apache
[[ ! -f /app/data/apache/mpm_prefork.conf ]] && cp /app/pkg/mpm_prefork.conf /app/data/apache/mpm_prefork.conf

echo "==> run migration"
$occ upgrade
# https://docs.nextcloud.com/server/15/admin_manual/configuration_database/bigint_identifiers.html#bigint-64bit-identifiers
$occ db:convert-filecache-bigint --no-interaction
# the RewriteBase requires additional htaccess rules that this command will generate
$occ maintenance:update:htaccess
# mimetype migration
$occ maintenance:repair --include-expensive

# patch htaccess to contain full url (see #59). this code does not re-patch across restarts
sed -e 's,caldav /,caldav https:\/\/%{HTTP_HOST}/,' \
    -e 's,carddav /,carddav https:\/\/%{HTTP_HOST}/,' -i /app/data/htaccess

# To be removed on the next release
echo "==> Disabling LDAP"
$occ app:disable user_ldap || true

# OIDC
if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "==> Ensure OIDC settings"

    $occ app:install user_oidc || true

    # --group-provisioning=0_or_1 is intentionally not set, this is up to the user
    $occ user_oidc:provider "Cloudron" --clientid="${CLOUDRON_OIDC_CLIENT_ID}" --clientsecret="${CLOUDRON_OIDC_CLIENT_SECRET}" \
        --discoveryuri="${CLOUDRON_OIDC_DISCOVERY_URL}" --scope="openid email profile groups" --mapping-groups="groups" \
        --unique-uid=0 --mapping-uid=sub
fi

# add indices of the share table (see #61)
$occ db:add-missing-indices
$occ db:add-missing-columns

# this sometimes fails due to existing duplicate fileids in the oc_filecache_extended table so we have to apply that workaround
if ! $occ db:add-missing-primary-keys; then
    echo "==> Applying oc_filecache_extended workaround"
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "CREATE TABLE foobar AS SELECT fileid, MAX(metadata_etag) AS metadata_etag, MAX(creation_time) AS creation_time, MAX(upload_time) AS upload_time FROM oc_filecache_extended GROUP BY fileid;"
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "ALTER TABLE oc_filecache_extended RENAME TO save_oc_filecache_extended;"
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "ALTER TABLE foobar RENAME TO oc_filecache_extended;"
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "DROP TABLE save_oc_filecache_extended;"

    $occ db:add-missing-primary-keys
fi

# now disable maintenance mode in case it was set
$occ maintenance:mode --off

# chown carefully for large data. the flag currently protects against restarts but not updates (since /run is cleaned up on an update)
echo "==> Changing ownership"
chown -R www-data.www-data /run/nextcloud
if [[ ! -f /run/nextcloud/chowned ]]; then
    # On app update, /run is cleared but file prems are already correct. On app restore (rsync), /run is cleared and file perms are wrong
    # test the permission of php.ini (one of the last files) - this does break if we have users after 'p' in legacy datadirectory
    if [[ "$(stat -c '%U' /app/data/php.ini)" != "www-data" ]]; then
        find /app/data \( -type f -or -type d \) -and ! -user www-data -exec chown www-data:www-data '{}' \+
    fi
fi

touch /run/nextcloud/chowned

# turn configuration (the install and enable command work properly only after chowning)
if [[ -n "${CLOUDRON_TURN_SERVER:-}" ]]; then
    echo "==> Installing and enabling spreed, if needed"
    $occ app:install spreed || true
    $occ app:enable spreed || true

    $occ config:app:set spreed stun_servers --value "[\"${CLOUDRON_STUN_SERVER}:${CLOUDRON_STUN_PORT}\"]"
    $occ config:app:set spreed turn_servers --value "[{\"server\":\"${CLOUDRON_TURN_SERVER}:${CLOUDRON_TURN_PORT}\",\"secret\":\"${CLOUDRON_TURN_SECRET}\",\"protocols\":\"udp,tcp\"}]"
fi

# without this, nc will show "Internal server error" on start up
echo "==> Run cron job on startup"
/usr/local/bin/gosu www-data:www-data php -f /app/code/cron.php

echo "==> Start NextCloud"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
