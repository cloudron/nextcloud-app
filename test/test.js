#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global afterEach */
/* global it */
/* global xit */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    request = require('request'),
    manifest = require('../CloudronManifest.json'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';
    const LOCAL_FILENAME = 'sticker.png';
    const REMOTE_FILENAME = 'sticker.png';

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const adminUser = 'admin';
    const adminPassword = 'changeme';
    let athenticated_by_oidc = false;

    let app, browser;
    let host_os;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1280 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    function testFileDownload(username, password, callback) {
        var data = {
            url: `https://${app.fqdn}/remote.php/webdav/${REMOTE_FILENAME}`,
            auth: { username: username, password: password },
            encoding: 'binary'
        };

        request.get(data, function (error, response, body) {
            if (error !== null) return callback(error);
            if (response.statusCode !== 200) return callback('Status code: ' + response.statusCode);
            if (body !== fs.readFileSync(path.resolve(LOCAL_FILENAME)).toString('binary')) return callback('File corrupt');

            callback(null);
        });
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password) {
        await browser.get(`https://${app.fqdn}/login`);
        await browser.wait(until.elementLocated(By.id('user')), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(By.id('user'))), TEST_TIMEOUT);
        await browser.findElement(By.id('user')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await browser.sleep(10000);
        await waitForElement(By.xpath('//*[@id="contactsmenu"]'));
    }

    async function loginOIDC(username, password) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);

        await waitForElement(By.xpath('//a[contains(@href, "user_oidc") and contains(., "Login with Cloudron")]'));
        await browser.findElement(By.xpath('//a[contains(@href, "user_oidc") and contains(., "Login with Cloudron")]')).click();

        if (!athenticated_by_oidc) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
            athenticated_by_oidc = true;
        }

        const cookies = await browser.manage().getCookies();
        const cookie = cookies.filter(function (c) { return c.name === '_gitlab_session'; })[0];

        await waitForElement(By.xpath('//*[@id="contactsmenu"]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//*[@id="user-menu"]'));
        await browser.findElement(By.xpath('//*[@id="user-menu"]')).click();
        await browser.sleep(3000);
        await waitForElement(By.xpath('//a[contains(@href, "/logout") or contains(@href, "user_oidc")]'));
        await browser.findElement(By.xpath('//a[contains(@href, "/logout") or contains(@href, "user_oidc")]')).click();
        await waitForElement(By.xpath('//input[@name="user"]'));
    }

    async function closeWizard() {
        await browser.get(`https://${app.fqdn}`);

        await waitForElement(By.xpath('//div[@id="firstrunwizard"]//div[@class="modal-container__content"]//button[@aria-label="Close"]'));
        await browser.findElement(By.xpath('//div[@id="firstrunwizard"]//div[@class="modal-container__content"]//button[@aria-label="Close"]')).click();
        await waitForElement(By.id('app-dashboard'));

        // give it some time to save
        await browser.sleep(2000);
    }

    async function listUsers() {
        await browser.get(`https://${app.fqdn}/settings/users`);
        await waitForElement(By.xpath('//div[@class="user-list-grid"]//div[contains(text(), "admin")]'));
        await waitForElement(By.xpath(`//div[@class="user-list-grid"]//div[contains(text(), "${username}")]`));
    }

    async function checkSetupWarnings() {
        await browser.get(`https://${app.fqdn}/settings/admin/overview`);
        await waitForElement(By.xpath('//span[contains(text(), "All checks passed.")]'));
    }

    async function checkFile(filename) {
        await browser.get(`https://${app.fqdn}/apps/files`);
        await waitForElement(By.xpath(`//span[text()="${filename}"]`));
    }

    function uploadFile(username, password) {
        const cmd = `curl --insecure --http1.1 -X PUT -u ${username}:${password} "https://${app.fqdn}/remote.php/dav/files/${username}/${REMOTE_FILENAME}" --data-binary @"./${LOCAL_FILENAME}"`;
        execSync(cmd);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no sso
    it('install app (NO SSO)', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login as admin', login.bind(null, adminUser, adminPassword));
    it('can close wizard', closeWizard);

    it('can upload file', uploadFile.bind(null, adminUser, adminPassword));
    it('can check file', checkFile.bind(null, 'sticker'));
    it('can download previously uploaded file', testFileDownload.bind(null, adminUser, adminPassword));
    it('can logout', logout);
    it('uninstall app', async function () {
        await browser.get('about:blank'); // ensure we don't hit NXDOMAIN in the mean time
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // sso
    it('install app (SSO)', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('can close the wizard', closeWizard);
    it('can logout', logout);

    it('can login as admin', login.bind(null, adminUser, adminPassword));
    it('can close wizard', closeWizard);
    it('can upload file', uploadFile.bind(null, adminUser, adminPassword));
    it('can check file', checkFile.bind(null, 'sticker'));
    it('can download previously uploaded file', testFileDownload.bind(null, adminUser, adminPassword));

    xit('can list users', listUsers);
    xit('has no setup warnings', checkSetupWarnings);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('can check file', checkFile.bind(null, 'Readme'));
    it('can logout', logout);

    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('can check file', checkFile.bind(null, 'sticker'));
    it('can download previously uploaded file', testFileDownload.bind(null, adminUser, adminPassword));
    xit('can list users', listUsers);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('can check file', checkFile.bind(null, 'Readme'));
    it('can logout', logout);

    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('can check file', checkFile.bind(null, 'sticker'));
    it('can download previously uploaded file', testFileDownload.bind(null, adminUser, adminPassword));
    xit('can list users', listUsers);
    xit('has no setup warnings', checkSetupWarnings);
    it('can logout', logout);

    it('move to different location', async function () {
        browser.manage().deleteAllCookies();
        await browser.get('about:blank'); // ensure we don't hit NXDOMAIN in the mean time

        execSync(`cloudron configure --app ${app.id} --location ${LOCATION}2`, EXEC_ARGS);
        getAppInfo();
    });


    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('can check file', checkFile.bind(null, 'Readme'));
    it('can logout', logout);

    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('can check file', checkFile.bind(null, 'sticker'));
    it('can download previously uploaded file', testFileDownload.bind(null, adminUser, adminPassword));
    xit('can list users', listUsers);
    xit('has no setup warnings', checkSetupWarnings);

    it('uninstall app', async function () {
        await browser.get('about:blank'); // ensure we don't hit NXDOMAIN in the mean time
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id com.nextcloud.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('can close the wizard', closeWizard);
    it('can upload file', uploadFile.bind(null, adminUser, adminPassword));
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --no-backup --app ${LOCATION}`, EXEC_ARGS); });

    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('can close the wizard', closeWizard);
    it('can check file', checkFile.bind(null, 'Readme'));
    it('can logout', logout);

    it('can admin login', login.bind(null, adminUser, adminPassword));
    xit('can close the wizard', closeWizard);
    it('can check file', checkFile.bind(null, 'sticker'));
    it('can download previously uploaded file', testFileDownload.bind(null, adminUser, adminPassword));
    xit('has no setup warnings', checkSetupWarnings);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank'); // ensure we don't hit NXDOMAIN in the mean time
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
