[0.1.0]
* Initial release

[0.1.1]
* Update tagline

[0.2.0]
* Update to latest upstream versin 10.0.1

[0.2.1]
* Support optional SSO

[0.3.0]
* Update to upstream version 11.0.0

[0.3.1]
* Make upload size configurable

[0.3.2]
* Update to upstream version 11.0.1

[0.3.3]
* Hotfix release for 11.0.1

[0.3.4]
* Update to 11.0.2

[0.3.5]
* Fix description

[0.4.0]
* Update to base image 0.10.0
* Install smbclient for external storage addon
* Use upstream setupchecks

[0.4.1]
* Update to upstream version 11.0.3

[0.5.0]
* Update to upstream version 12.0.0

[0.5.1]
* Enable and configure PHP opcache

[0.5.2]
* Cleanup session data

[1.0.0]
* Update to upstream version 12.0.1

[1.0.1]
* Update to upstream version 12.0.2

[1.0.2]
* Update to upstream version 12.0.3

[1.0.3]
* Use release channel 'cloudron'

[1.0.4]
* Bring down max idle processes to 3 to conserve memory when app is not in use

[1.1.0]
* Update NextCloud to 12.0.4
* Add documentationUrl

[1.2.0]
* Ensure all installations have the oc_ prefix for db tables

[1.3.0]
* Update NextCloud to 12.0.5

[2.0.0]
* Update to Nextcloud 13

[2.0.1]
* Update to NextCloud 13.0.1

[2.1.0]
* Update NextCloud to 13.0.2

[2.1.1]
* Update apache worker config

[2.1.2]
* Update NextCloud to 13.0.3

[2.1.3]
* Update NextCloud to 13.0.4

[2.1.4]
* Update NextCloud to 13.0.5

[2.1.5]
* Update NextCloud to 13.0.6

[2.2.0]
* Update base image

[2.2.1]
* Update NextCloud to 13.0.7

[3.0.0]
* Update NextCloud to 14.0.4

[4.0.0]
* Update NextCloud to 15.0.2

[4.0.1]
* Update NextCloud to 15.0.4

[4.0.2]
* Fix caldav and cardav redirection
* Add share table indices

[4.0.3]
* Update NextCloud to 15.0.5

[4.0.4]
* Update NextCloud to 15.0.6

[4.0.5]
* Update NextCloud to 15.0.7

[4.1.0]
* Update NextCloud to 16.0.0

[4.1.1]
* Increase the php memory limit to recommended value of 512MB

[4.1.2]
* Update NextCloud to 16.0.1

[4.1.3]
* Update NextCloud to 16.0.2

[4.1.4]
* Update NextCloud to 16.0.3
* Fixes drag'n'drop upload regression

[4.1.5]
* Update Nextcloud to 16.0.4

[4.2.0]
* Update Nextcloud to 17.0.0

[4.2.1]
* Update Nextcloud to 17.0.1

[4.2.2]
* Update Nextcloud to 17.0.2

[4.3.0]
* Update Nextcloud to 18.0.0

[4.3.1]
* Provide support for rar and 7zip archives

[4.3.2]
* Update Nextcloud to 18.0.1

[4.3.3]
* Update Nextcloud to 18.0.2
* Install imagemagik and ghostscript for previews
* Install latest unrar 5.8.0

[4.4.0]
* Pre-install Talk app
* Pre-provision with TURN credentials

[4.4.1]
* Update Nextcloud to 18.0.4

[4.5.0]
* Use latest base image 2.0.0

[4.5.1]
* Add missing imagick support

[4.6.0]
* Update Nextcloud to 19.0.0

[4.6.1]
* Update Nextcloud to 19.0.1

[4.6.2]
* Add package hook to create missing database columns if needed

[4.6.3]
* Update Nextcloud to 19.0.2

[4.6.4]
* Update Nextcloud to 19.0.3

[4.6.5]
* Update Nextcloud to 19.0.4

[4.7.0]
* Update Nextcloud to 20.0.1

[4.8.0]
* Use username as uid for better portability across Cloudrons

[4.8.1]
* Update Nextcloud to 20.0.2
* Ensure primary keys in the db are created. Nextcloud db migration does not do this!

[4.8.2]
* Update Nextcloud to 20.0.2
* Ensure primary keys are created and apply potential db [fix](https://forum.cloudron.io/topic/3827/nextcloud-update-to-20-0-2-failed/8?_=1606861857596)

[4.8.3]
* Update Nextcloud to 20.0.3
* [Full changelog](https://nextcloud.com/changelog/#latest20)

[4.8.4]
* Update Nextcloud to 20.0.4
* [Full changelog](https://nextcloud.com/changelog/#latest20)

[4.8.5]
* Update Nextcloud to 20.0.5
* [Full changelog](https://nextcloud.com/changelog/#latest20)

[4.8.6]
* Update Nextcloud to 20.0.6
* [Full changelog](https://nextcloud.com/changelog/#latest20)

[4.8.7]
* Update Nextcloud to 20.0.7
* [Full changelog](https://nextcloud.com/changelog/#latest20)

[4.8.8]
* Update Nextcloud to 20.0.8
* Use base image v3
* [Full changelog](https://nextcloud.com/changelog/#latest20)

[4.8.9]
* Update Nextcloud to 20.0.9
* [Full changelog](https://nextcloud.com/changelog/#latest20)

[4.9.0]
* Update Nextcloud to 21.0.1
* [Full changelog](https://nextcloud.com/changelog/#latest21)

[4.9.1]
* Update Nextcloud to 21.0.2
* [Full changelog](https://nextcloud.com/changelog/#latest21)

[4.9.2]
* Update Nextcloud to 21.0.3
* [Full changelog](https://nextcloud.com/changelog/#latest21)

[4.10.0]
* Update Nextcloud to 22.0.0
* [Full changelog](https://nextcloud.com/changelog/#latest22)

[4.10.1]
* Update Nextcloud to 22.1.0
* [Full changelog](https://nextcloud.com/changelog/#latest22)

[4.10.2]
* Update Nextcloud to 22.1.1
* [Full changelog](https://nextcloud.com/changelog/#latest22)

[4.11.0]
* Update Nextcloud to 22.2.0
* [Full changelog](https://nextcloud.com/changelog/#latest22)
* Update unrar to 6.0.2

[4.11.1]
* Update Nextcloud to 22.2.1
* [Full changelog](https://nextcloud.com/changelog/#latest22)

[4.11.2]
* Update Nextcloud to 22.2.2
* [Full changelog](https://nextcloud.com/changelog/#latest22)

[4.11.3]
* Update Nextcloud to 22.2.3
* [Full changelog](https://nextcloud.com/changelog/#latest22)
* Bump moment-timezone from 0.5.33 to 0.5.34 (server#29658)
* Don't flash external storage mountpoints during the status check (server#29706)
* Bump doctrine/dbal to 3.1.4 (server#29717)
* Add composer patch (3rdparty#890)
* Bump doctrine/dbal to 3.1.4 (3rdparty#895)

[4.11.4]
* Update Nextcloud to 22.2.4
* [Full changelog](https://nextcloud.com/changelog/#latest22)
* Bump moment-timezone from 0.5.33 to 0.5.34 (server#29658)
* Don't flash external storage mountpoints during the status check (server#29706)
* Bump doctrine/dbal to 3.1.4 (server#29717)
* Add composer patch (3rdparty#890)
* Bump doctrine/dbal to 3.1.4 (3rdparty#895)

[4.12.0]
* Update Nextcloud to 23.0.0
* [Full changelog](https://nextcloud.com/changelog/#latest23)

[4.12.1]
* Update Nextcloud to 23.0.2
* [Full changelog](https://nextcloud.com/changelog/#latest23)

[4.12.2]
* Update Nextcloud to 23.0.3
* [Full changelog](https://nextcloud.com/changelog/#latest23)

[4.12.3]
* Update Nextcloud to 23.0.4
* [Full changelog](https://nextcloud.com/changelog/#latest24)

[4.13.0]
* Update Nextcloud to 24.0.1
* [Full changelog](https://nextcloud.com/changelog/#latest24)

[4.13.1]
* Update Nextcloud to 24.0.2
* [Full changelog](https://nextcloud.com/changelog/#latest24)

[4.13.2]
* Update Nextcloud to 24.0.3
* [Full changelog](https://nextcloud.com/changelog/#latest24)

[4.14.0]
* Update Nextcloud to 24.0.4
* [Full changelog](https://nextcloud.com/changelog/#latest24)
* Optimize data directory chown and do not chown on restarts
* Change default data directory for new installations to `/app/data/data`

[4.14.1]
* Fix LDAP config to expose groups for including in NextCloud

[4.14.2]
* Update Nextcloud to 24.0.5
* [Full changelog](https://nextcloud.com/changelog/#latest24)
* Recover installation when creating the database user fails and improve password strength (server#33514)
* Unify initial and updated quota display (server#33538)
* Fix updating size when folder is empty (server#33562)
* Optimize search post-processing for jail wrapper (server#33603)
* Further pre-filter search result before setting up share source cache (server#33604)
* Fix encryption:fix-encrypted-version command when encrypted is set to 0 (server#33636)
* Bump marked from 4.0.13 to 4.0.19 (server#33642)
* Respect user settings in php.ini if they are big enough (server#33644)
* Improve handling of profile page (server#33648)
* Fix carddav activities (server#33651)
* Directly build the search filter for shared storage instead of setting up the source cache (server#33656)
* Update psalm-baseline.xml (server#33663)
* Fix plural usage in LDAP wizard (server#33667)
* Update CRL (server#33676)
* Logger ignore args of sharepoint-related methods (server#33689)
* Add back TokenCleanupJob to invalidate old temporary tokens (server#33696)
* Fix copy in view-only mode (server#33720)
* Remove leading slash for search results at mountpoint root (server#33749)
* Check calendar URI length before creation (server#33782)
* Compare lowercase email when updating from ldap (server#33920)
* Revert “Compare lowercase email when updating from ldap” (server#33935)
* Update christophwurst/nextcloud dependency (activity#883)
* Update .l10nignore (activity#885)
* Fix translation ignore (activity#889)
* Update christophwurst/nextcloud dependency (activity#896)
* Ignore remnants (circles#1128)
* Update christophwurst/nextcloud dependency (circles#1139)
* Fix background email job for disabled users (notifications#1256)
* Fix duplicate ID on settings page (notifications#1262)
* Fix service-worker (photos#1214)
* Revert “fix service-worker” (photos#1217)
* Build(deps-dev): bump eslint-plugin-jsdoc from 39.3.4 to 39.3.6 (text#2800)
* Build(deps): bump prosemirror-markdown from 1.9.3 to 1.9.4 (text#2820)
* Revert “build(deps): bump prosemirror-markdown from 1.9.3 to 1.9.4” (text#2870)

[4.14.3]
* Update Nextcloud to 24.0.6
* [Full changelog](https://nextcloud.com/changelog/#latest24)
* Bump moment from 2.29.3 to 2.29.4 (server#33423)
* Bump @nextcloud/dialogs from 3.1.2 to 3.1.4 (server#33461)
* Make groupfolders use system wide encryption keys (server#33700)
* Log if cookie login failed with token mismatch or session unavailability (server#33787)
* Wait for the new user form to be visible in acceptance tests (server#33791)
* Fix handling of internal errors when uploading theming files (server#33798)
* 33036 [Bug]: Set default expiration date (server#33805)
* Always use the default fs owner when storing versions (server#33970)
* Improve getting recent files performance (server#33983)
* Do not empty config.php file if reading failed for any reason (server#34021)
* Do not output a warning when a file is not found (server#34089)
* Convert file_metadata.id from intto bigint (server#34090)
* Reset global search on files sidebar navigation change (server#34106)
* Dont try email login if the provider username is not a valid email (server#34111)
* Fix translation of user exists error server-side (server#34112)
* Update ca-cert bundle (server#34128)
* Clear search results when remove search query (server#34159)
* Show long names correctly on profile card (server#34228)
* Bump moment-timezone from 0.5.34 to 0.5.37 (server#34259)
* Correctly handle Redis::keys returning false (server#34341)
* Fix empty content of dashboard (activity#924)
* Update christophwurst/nextcloud dependency (activity#931)
* Migrate to nextcloud/OCP package in stable24 (activity#937)
* Update nextcloud/ocp dependency (activity#942)
* Migrate to nextcloud/OCP package in stable24 (firstrunwizard#746)
* Migrate to nextcloud/OCP package in stable24 (logreader#792)
* Migrate to nextcloud/OCP package in stable24 (nextcloud_announcements#110)
* Update nextcloud/ocp dependency (nextcloud_announcements#114)
* Fix checking for links finally (notifications#1277)
* Update christophwurst/nextcloud dependency (notifications#1284)
* Migrate to nextcloud/OCP package in stable24 (notifications#1294)
* Update nextcloud/ocp dependency (notifications#1297)
* Revert “Revert “fix service-worker”” (photos#1220)
* Migrate to nextcloud/OCP package in stable24 (serverinfo#403)
* Migrate to nextcloud/OCP package in stable24 (survey_client#149)
* Build(deps-dev): bump @cypress/webpack-preprocessor from 5.12.0 to 5.12.2 (text#2819)
* Append a newline after inserted image (text#2864)
* Fix: update link menububble on editor updates (text#2874)
* Update: tiptap and prosemirror (text#2909)
* Fix emoji autocompletion keyboard behaviour (text#2911)
* Fixes focus handling in MenuBar (text#2916)
* Align popover menus to the left on mobile (text#2917)

[4.15.0]
* Update Nextcloud to 25.0.1
* [Full changelog](https://nextcloud.com/changelog/#latest_hub_3)
* This update brings Nextcloud Hub 3

[4.16.0]
* Update Nextcloud to 25.0.2
* [Full changelog](https://nextcloud.com/changelog/#latest_hub_3)
* Upgrade PHP version to 8.1. Please check for compatibility when upgrading

[4.16.1]
* Install rar from apt
* Do not re-install spreed if it's disabled/uninstalled

[4.16.2]
* Update Nextcloud to 25.0.3
* [Full changelog](https://nextcloud.com/changelog/#latest25)

[4.16.3]
* Update Nextcloud to 25.0.4
* [Full changelog](https://nextcloud.com/changelog/#latest25)

[4.16.4]
* Update Nextcloud to 25.0.5
* [Full changelog](https://nextcloud.com/changelog/#latest25)

[4.17.0]
* Update NExtcloud to 26.0.1
* [Full changelog](https://nextcloud.com/changelog/#latest26)

[4.17.1]
* Update Nextcloud to 26.0.2
* [Full changelog](https://nextcloud.com/changelog/#latest26)

[4.17.2]
* Remove table rename migration logic (which breaks when postgis is enabled)

[4.17.3]
* Update Nextcloud to 26.0.3
* [Full changelog](https://nextcloud.com/changelog/#26-0-3)

[4.17.4]
* Update Nextcloud to 26.0.4
* [Full changelog](https://nextcloud.com/changelog/#26-0-4)

[4.18.0]
* Update Nextcloud to 27.0.1
* [Full changelog](https://nextcloud.com/changelog/#27-0-1)

[4.18.1]
* Update Nextcloud to 27.0.2
* [Full changelog](https://nextcloud.com/changelog/#27-0-2)

[4.18.2]
* Do not set disable debug flag

[4.19.0]
* Optional turn support

[4.20.0]
* Update Nextcloud to 27.1.0
* [Full changelog](https://nextcloud.com/changelog/#27-1-0)

[4.20.1]
* Update Nextcloud to 27.1.0
* [Full changelog](https://github.com/nextcloud/server/releases/tag/v27.1.0)

[4.20.2]
* Update Nextcloud to 27.1.1
* [Full changelog](https://github.com/nextcloud/server/releases/tag/v27.1.1)
* fix(mimetype): Fix aborted transaction on PostgreSQL when storing mimetype by @backportbot-nextcloud in #40461
* Backport consider link shares in removeShare method in SharingTab #40440 by @Fenn-CS in #40448
* don't preload metadata for the sub-childen by @backportbot-nextcloud in #40473
* fix: don't use davtags for tag search by @backportbot-nextcloud in #40444
* fix: use faster method to fetch user count by @backportbot-nextcloud in #40433
* SFTP improvements by @backportbot-nextcloud in #40487
* fix(comments): Use provided offset in best effort when loading comments by @backportbot-nextcloud in #40506
* Fix alignments for share different share entry types. by @Fenn-CS in #40429
* fix: content info footer on guest pages by @st3iny in #40520
* Consider share type for enforced share expiry dates by @Fenn-CS in #40530
* Auto set password for new email shares by @Fenn-CS in #40535
* Fix issues where unencrypted_size was being falsely used for non-encrypted home folders by @juliushaertl in #40379

[4.20.3]
* Update Nextcloud to 27.1.2
* Update base image to 4.2.0
* [Full changelog](https://github.com/nextcloud/server/releases/tag/v27.1.2)
* [stable27] ocm services by @ArtificialOwl in #40586
* [stable27] chore: Update @nextcloud/dialogs to 4.2.1 by @susnux in #40557
* [stable27] Let occ trashbin:restore restore also from groupfolders and add filters by @backportbot-nextcloud in #40581
* [stable27] Revert change that made OC.Util.humanFileSize return decimal sizes instead of binary by @susnux in #40605
* [stable27] Store size in int|float for 32bit support by @backportbot-nextcloud in #40623
* [stable27] Fix jsunit tests by @susnux in #40638
* [stable27] Use proper app id in Version.vue by @backportbot-nextcloud in #40640
* [stable27] fix: encode uri for trashbin delete by @kesselb in #40642
* [stable27] fix(dav): expand recurrences when searching by @backportbot-nextcloud in #40631
* [stable27] fix ocm-provider rewrite rules by @blizzz in #40778

[4.20.4]
* Update Nextcloud to 27.1.3
* [Full changelog](https://github.com/nextcloud/server/releases/tag/v27.1.3)

[4.20.5]
* Update Nextcloud to 27.1.4
* [Full changelog](https://github.com/nextcloud/server/releases/tag/v27.1.4)

[4.20.6]
* Update Nextcloud to 27.1.5
* [Full changelog](https://github.com/nextcloud/server/releases/tag/v27.1.5)

[4.21.0]
* Update Nextcloud to 28.0.2
* [Full changelog](https://github.com/nextcloud/server/releases/tag/v28.0.2)

[4.21.1]
* Update Nextcloud to 28.0.3
* [Full changelog](https://nextcloud.com/changelog/#28-0-3)

[4.21.2]
* Update NextCloud to 28.0.4
* [Full changelog](https://nextcloud.com/changelog/#28-0-4)

[4.21.3]
* Fix LDAP user group mapping

[4.21.4]
* Update NextCloud to 28.0.5
* [Full changelog](https://nextcloud.com/changelog/#28-0-5)

[4.21.5]
* Update NextCloud to 28.0.6
* [Full changelog](https://nextcloud.com/changelog/#28-0-6)

[4.22.0]
* Update NextCloud to 29.0.1
* [Full changelog](https://nextcloud.com/changelog/#29-0-0)

[4.22.1]
* Update NextCloud to 29.0.2
* [Full changelog](https://nextcloud.com/changelog/#29-0-2)

[4.22.2]
* Update NextCloud to 29.0.3
* [Full changelog](https://nextcloud.com/changelog/#29-0-3)

[4.22.3]
* Update NextCloud to 29.0.4
* [Full changelog](https://nextcloud.com/changelog/#29-0-4)

[4.22.4]
* Update NextCloud to 29.0.7
* [Full changelog](https://nextcloud.com/changelog/#latest29)

[4.22.5]
* Update NextCloud to 29.0.8
* [Full changelog](https://nextcloud.com/changelog/#latest29)

[4.23.0]
* Update NextCloud to 30.0.1
* [Full changelog](https://nextcloud.com/changelog/#latest30)

[4.23.1]
* Update Nextcloud to 30.0.2
* [Full Changelog](https://nextcloud.com/changelog/#latest30)
* \[stable30] fix: Make user removal more resilient by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48589
* \[stable30] fix(files): Reset context menu position on close by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/47878
* \[stable30] fix(files): Add more visual move / copy notification by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48458
* \[stable30] fix: get rid of denied notification when accept by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48751
* \[stable30] fix(share): Return empty string if no label is set by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48703
* \[stable30] fix(files): Ensure children are removed from folder and not duplicated by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48240
* \[stable30] feat(comments): Support mentioning emails by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48785
* \[stable30] fix(appstore): Hide last modified information for shipped apps by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48754
* \[stable30] fix(security): Update code signing revocation list by [@&#8203;nextcloud-command](https://github.com/nextcloud-command) in https://github.com/nextcloud/server/pull/48778
* \[stable30] fix(activity): Fix download activity parameters by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48818
* \[stable30] fix(AppConfig): Add external JWT private key to sensitive keys by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48710
* \[stable30] build: Bump symfony/\* to 6.4 by [@&#8203;nickvergessen](https://github.com/nickvergessen) in https://github.com/nextcloud/server/pull/48763
* \[stable30] fix(files_sharing): Add proper user facing messages on success by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48730
* \[stable30] ci: Update list of installed PHP extensions (as some are not default  by [@&#8203;nickvergessen](https://github.com/nickvergessen) in https://github.com/nextcloud/server/pull/48824
* \[stable30] fix(files): Ensure renaming state is correctly reset by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48731
* \[stable30] fix(filesexternal): Remove unneeded 3rdparty library use by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48550
* \[stable30] fix(logger): Remove more parameters of other methods by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48830
* \[stable30] fix: add PasswordConfirmationRequired to create user storages endpoint by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48748
* \[stable30] fix(app-store): Also proxy images of locally installed apps by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48868
* \[stable30] fix(app-store): Ensure the `groups` property is always an array by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48866
* \[stable30] fix(files): Adjust NavigationQuota for Nextcloud 30 design by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48834
* \[stable30] fix(files_sharing): Add correct context to translation by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48836
* \[stable30] \[TextProcessing] Fix: Also list types that are available in task processing by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48877
* \[stable30] fix(app-store): Correctly render Markdown in app description by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48872
* \[stable30] build: Print RTL limited characters in translation-checker by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48856
* \[stable30] fix(ShareAPI): Send mails for mail shares by default by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48881
* \[stable30] refactor(ShareApiController): Check for null and empty strings with e by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48890
* \[stable30] fix(files): handle empty view with error by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48893
* \[stable30] fix(files): add title for files list table header button by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48901
* \[stable30] fix: Allow overriding shouldApplyQuota check from child classes by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48635
* \[stable30] fix(dav): Cleanup view-only check by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48931
* \[stable30] fix(files_sharing): Cleanup error messages by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48937
* chore(deps): Bump guzzlehttp/guzzle from 7.8.1 to 7.8.2 by [@&#8203;nickvergessen](https://github.com/nickvergessen) in https://github.com/nextcloud/server/pull/48896
* \[stable30] fix: encrypt and store password, decrypt and retrieve the same by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48939
* \[stable30] fix(unified-search): Close on second ctrl+f by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48861
* \[stable30] fix(config): Mark more configs as sensitive by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48950
* \[stable30] fix(files_sharing): federated shares avatar by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48888
* \[stable30] fix(app-store): Update update count in navigation by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48945
* \[stable30] ci(psalm): Add missing imagick extension by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48957
* \[stable30] fix: do not reduce count for subadmins if they are members of group by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48571
* \[stable30] fix: Fix "Unknown path" error when source user `files` folder has not been initialized by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48746
* \[stable30] fix: add PasswordConfirmationRequired to the external storages mentioned in review by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48976
* \[stable30] Fix copying or moving from shared groupfolders by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48984
* \[stable30] fix(files): Do not jump to top when fileId is set to currentFolder by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48979
* \[stable30] fix: Shipped apps should include the Nextcloud version in the cache buster by [@&#8203;susnux](https://github.com/susnux) in https://github.com/nextcloud/server/pull/48701
* \[stable30] Fix disabled user list for subadmins by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48970
* chore(deps): Bump libphonenumber-js from 1.11.9 to 1.11.11 by [@&#8203;dependabot](https://github.com/dependabot) in https://github.com/nextcloud/server/pull/48667
* \[stable30] fix: provision api's status codes by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48474
* \[stable30] fix: Return correct list of managers for a user by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48622
* \[stable30] fix: Fix empty sections appearing in search results by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48624
* \[stable30] chore: Replace Twitter & Diaspora links and text by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48620
* \[stable30] fix: improve moving object store items to trashbin by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48245
* \[stable30] fix: Fix "Reasons to use Nextcloud" design by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48652
* \[stable30] fix(locking): Accept mixed as value on setTTL by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48694
* \[stable30] fix: add PasswordConfirmationRequired to saveGlobalCredentials by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49005
* \[stable30] Fix email share transfer accross storages by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48618
* \[stable30] fix(users): improve recently active search by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49001
* \[stable30] fix(FileList): Show correct avatar for federated share owner by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48294
* \[stable30] fix(files_sharing): Password field must not be required if already set by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49013
* \[stable30] feat(settings): migrate AppAPI ExApps management to settings by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/48988
* \[stable30] fix(files-external): set password as sensitive by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49028
* \[stable30] fix(ShareEntryLinkList): Append new links to the end of list by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49022
* 30.0.2 RC1 by [@&#8203;Altahrim](https://github.com/Altahrim) in https://github.com/nextcloud/server/pull/48996
* \[stable30] chore: Update code owners by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49057
* \[stable30] fix(apps-store): Fix exception on generating preview url for installed app screenshot by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49044
* \[stable30] fix(owncloud): Fix ownCloud migration with oauth2 app by [@&#8203;nickvergessen](https://github.com/nickvergessen) in https://github.com/nextcloud/server/pull/49076
* \[stable30] Update `@nextcloud/dialogs` to v6.0.1 by [@&#8203;susnux](https://github.com/susnux) in https://github.com/nextcloud/server/pull/49093
* \[stable30] fix(caldav): broken activity rich objects by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49080
* \[stable30] docs: update overwrite.cli.url wording by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49067
* 30.0.2 RC2 by [@&#8203;Altahrim](https://github.com/Altahrim) in https://github.com/nextcloud/server/pull/49079
* 30.0.2 by [@&#8203;Altahrim](https://github.com/Altahrim) in https://github.com/nextcloud/server/pull/49120

[4.23.2]
* Update server to 30.0.3
* [Full Changelog](https://nextcloud.com/changelog/#latest30)
* \[stable30] fix(files): multiselect and filters store declaration by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49136
* \[stable30] fix(theming): Return default theme if the user never selected a theme by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49138
* \[stable30] fix(TaskProcessingApiController): Don't use + to merge non-assoc. arrays by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49110
* \[stable30] fix(net/security): Handle IPv6 zone IDs in link-local addresses by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49107
* \[stable30] fix(files_sharing): note icon color on public pages by [@&#8203;skjnldsv](https://github.com/skjnldsv) in https://github.com/nextcloud/server/pull/49153
* \[stable30] fix(SharingEntryLink): Show default password before create if any by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49149

[4.23.3]
* Update server to 30.0.4
* [Full Changelog](https://nextcloud.com/changelog/#latest30)
* \[stable30] fix(app-store): Add back legacy store API used for update and removal by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49216
* \[stable30] fix(activity): make emails for link share uploads true by default by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49691
* chore(deps-dev): bump [@&#8203;nextcloud/cypress](https://github.com/nextcloud/cypress) from 1.0.0-beta.8 to 1.0.0-beta.12 by [@&#8203;dependabot](https://github.com/dependabot) in https://github.com/nextcloud/server/pull/49713
* chore(deps-dev): bump [@&#8203;babel/node](https://github.com/babel/node) from 7.25.0 to 7.25.9 by [@&#8203;dependabot](https://github.com/dependabot) in https://github.com/nextcloud/server/pull/49712
* chore(deps-dev): bump cypress-split from 1.24.0 to 1.24.7 by [@&#8203;dependabot](https://github.com/dependabot) in https://github.com/nextcloud/server/pull/49716
* chore(deps): bump pinia from 2.2.6 to 2.2.8 by [@&#8203;dependabot](https://github.com/dependabot) in https://github.com/nextcloud/server/pull/49715
* chore(deps): bump [@&#8203;nextcloud/sharing](https://github.com/nextcloud/sharing) from 0.2.3 to 0.2.4 by [@&#8203;dependabot](https://github.com/dependabot) in https://github.com/nextcloud/server/pull/49714
* \[stable30] fix(files_sharing): Stop infinite loop blocking link share requests by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49724
* 30.0.4 RC1 by [@&#8203;blizzz](https://github.com/blizzz) in https://github.com/nextcloud/server/pull/49737
* \[stable30] fix(files_sharing): Correct property enforced property names by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49809
* \[stable30] fix(files_sharing): also submit new share on password submit by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/49817
* 30.0.4 by [@&#8203;blizzz](https://github.com/blizzz) in https://github.com/nextcloud/server/pull/49810

[4.23.4]
* Update server to 30.0.5
* [Full Changelog](https://nextcloud.com/changelog/#latest30)
* [Chore(i18n): Improved grammar (server#49100)](https://github.com/nextcloud/server/pull/49100)
* [Fix metadata storage with sharding (server#49165)](https://github.com/nextcloud/server/pull/49165)
* [Chore(i18n): Fixed grammar (server#49556)](https://github.com/nextcloud/server/pull/49556)
* [Fix(files): virtual scroller item size computation (server#49561)](https://github.com/nextcloud/server/pull/49561)
* [FIX WebDav MacOS failed uploads php-fpm and big files (-36 error) (server#49562)](https://github.com/nextcloud/server/pull/49562)
* [Fix(maintenance): Show a success message on data-fingerprint command (server#49591)](https://github.com/nextcloud/server/pull/49591)
* [Fix(setupcheck): Make the Memcache setupcheck use the cache (server#49594)](https://github.com/nextcloud/server/pull/49594)

[5.0.0]
* Migrate from LDAP to OIDC
* **Important:** With the authentication provider change, you should use the `Login with Cloudron` button in the Nextcloud login screen. Nextcloud Clients on desktop and mobile may need re-authentication / re-setup .

[5.0.1]
* do not provision groups

[5.0.2]
* disable dns pinning

[5.1.0]
* Update server to 30.0.6
* [Full Changelog](https://nextcloud.com/changelog/#latest30)
* \[stable30] fix: template field title by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/50789
* \[stable30] fix(Mailer): Fix sendmail binary fallback by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/50777
* 30.0.6 by [@&#8203;Altahrim](https://github.com/Altahrim) in https://github.com/nextcloud/server/pull/50771

[5.1.1]
* Fix OIDC uid mapping

[5.1.2]
* Cleanup empty preview directories

[5.2.0]
* Update server to 31.0.0
* [Full Changelog](https://nextcloud.com/changelog/#latest30)
* \[stable31] fix: validate account properties as a repair step by [@&#8203;backportbot](https://github.com/backportbot) in https://github.com/nextcloud/server/pull/51005
* 31.0.0 by [@&#8203;blizzz](https://github.com/blizzz) in https://github.com/nextcloud/server/pull/51007
* 31.0.0 by [@&#8203;blizzz](https://github.com/blizzz) in https://github.com/nextcloud/server/pull/51011

[5.3.0]
* Update base image to 5.0.0
* Update PHPto 8.3

