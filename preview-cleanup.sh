#!/bin/bash

set -eu
set -o pipefail

echo "=> Run empty folder cleanup"

cd /app/code/
readonly instanceId=$(sudo -u www-data php occ config:system:get instanceid)

cd /app/data/data/appdata_${instanceId}/preview/
find . -type d -empty -delete
